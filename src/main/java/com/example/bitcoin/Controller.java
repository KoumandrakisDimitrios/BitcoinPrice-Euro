package com.example.bitcoin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class Controller{
    
    @Autowired
    private BitcoinService bitcoinService;

    @GetMapping(value = "/bitcoin")
    public String getBitcoinPrice() {
        return bitcoinService.getBitcoinPrice();
    }
}